var NODE_ENV = process.env.NODE_ENV;
var webpack = require('webpack');

module.exports = {
  entry: [
    // styles
    'bootstrap-css-only/css/bootstrap.css',
    './src/css/app.css',

    // scripts
    './src/js/app.js'
  ],

  output: {
    path: './build',
    filename: 'bundle.js'
  },

  module: {
    loaders: [
      {test: /\.js$/, include: /src/, loaders: ['babel']},
      {test: /\.css$/, loader: 'style!css'},
      {test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url?limit=100000'}
    ]
  },

  plugins: NODE_ENV === 'production' ? [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        properties: true,
        sequences: true,
        dead_code: true,
        conditionals: true,
        comparisons: true,
        evaluate: true,
        booleans: true,
        unused: true,
        loops: true,
        hoist_funs: true,
        cascade: true,
        if_return: true,
        join_vars: true,
        drop_console: true,
        drop_debugger: true,
        negate_iife: true,
        unsafe: true,
        hoist_vars: true,
      }
    }),
    new webpack.optimize.DedupePlugin()
  ] : []
};