var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var adapter = require('../storage-managers/ParseApiAdapter');
var util = require('../util/util');

module.exports = {
  validate: function(modelClass, key, value) {
    var errors = {};
    errors[key] = modelClass.check(key, value).message;

    AppDispatcher.dispatch({
      type: AppConstants.RECORD_VALIDATION_ERROR,
      errors: errors
    });
  },

  // emulate server validation for the demo
  validateAll: function (modelClass, data) {
    var obj = {};
    var errors = {};
    var isValid = true;

    // fill not presented fields with `undefined`
    Object.keys(modelClass.properties).forEach(function (prop) {
      obj[prop] = data[prop];
    });

    Object.keys(obj).forEach(function (chunk) {
      if (typeof data[chunk] !== 'object') {
        var errorMessage = modelClass.check(chunk, data[chunk]).message;
        if (errorMessage) {
          errors[chunk] = errorMessage;
          isValid = false;
        }
      }
    });

    return {
      errors: errors,
      isValid: isValid
    };
  },

  getRecords: function (modelClass) {
    var promise = adapter.retrieveAll(modelClass);
    var name = modelClass.name.toUpperCase();
    var data = Object.create({}, {
      model: {
        value: modelClass,
        enumeration: false
      }
    });

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_RECORDS,
      success: AppConstants.REQUEST_RECORDS_SUCCESS,
      failure: AppConstants.REQUEST_RECORDS_ERROR
    }, data);
  },

  createRecord: function (modelClass, data) {
    var modelProps = modelClass.properties;
    var stdIdName = util.getStdIdPropName(modelProps);
    var stdIdValue = data[stdIdName];

    // emulation of server validation
    var validationResult = this.validateAll(modelClass, data);

    if (!validationResult.isValid) {
      return AppDispatcher.dispatch({
        type: AppConstants.RECORD_VALIDATION_ERROR,
        errors: validationResult.errors
      });
    }

    adapter
      .retrieve(modelClass, '', stdIdValue)
      .then(function(records) {
        try {
          if (records.length) {
            AppDispatcher.dispatch({
              type: AppConstants.NON_UNIQUE
            });
          } else {
            data.runCloudCode = true;

            Object.defineProperty(data, 'model', {
              value: modelClass,
              enumerable: false
            });

            AppDispatcher.dispatchAsync(adapter.add(modelClass, data), {
              request: AppConstants.REQUEST_RECORD_SAVE,
              success: AppConstants.RECORD_SAVE_SUCCESS,
              failure: AppConstants.RECORD_SAVE_ERROR
            }, extendedData);
          }
        } catch(e) {}
      });

    AppDispatcher.dispatch({
      type: AppConstants.REQUEST_RECORD_SAVE
    });
  },

  updateRecord: function (modelClass, record, newData) {
    // emulation of server validation
    var newDataWithStdId = Object.assign({}, record, newData);
    var validationResult = this.validateAll(modelClass, newDataWithStdId);

    if (!validationResult.isValid) {
      return AppDispatcher.dispatch({
        type: AppConstants.RECORD_VALIDATION_ERROR,
        errors: validationResult.errors
      });
    }

    var promise;

    newData.runCloudCode = true;
    promise = adapter.update(modelClass, record, newData);
    newData.objectId = record.objectId;

    Object.defineProperty(newData, 'model', {
      value: modelClass,
      enumerable: false
    });

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_RECORD_UPDATE,
      success: AppConstants.RECORD_UPDATE_SUCCESS,
      failure: AppConstants.RECORD_UPDATE_ERROR
    }, newData);
  },

  deleteRecord: function (modelClass, record) {
    var promise;

    record.runCloudCode = true;

    Object.defineProperty(record, 'model', {
      value: modelClass,
      enumerable: false
    });

    promise = adapter.destroy(modelClass, record);

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_RECORD_DESTROY,
      success: AppConstants.RECORD_DESTROY_SUCCESS,
      failure: AppConstants.RECORD_DESTROY_ERROR
    }, record);
  },

  clearNotifications: function () {
    AppDispatcher.dispatch({
      type: AppConstants.CLEAR_NOTIFICATIONS
    });
  }
};