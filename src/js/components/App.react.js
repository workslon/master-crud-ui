var React = require('react');
var ReactRouter = require('react-router');
var AppStore = require('../stores/AppStore');
var AppActions = require('../actions/AppActions');
var StatusConstants = require('../constants/StatusConstants');
var NotificationStore = require('../stores/NotificationStore');
var IndexLink = ReactRouter.IndexLink;
var models = require('../models/Models');

module.exports = React.createClass({
  getInitialState: function () {
    var initialState = {
      notifications: NotificationStore.getNotifications()
    };

    models.forEach(function(model) {
      var name = model.name.toLowerCase() + 's';
      initialState[name] = AppStore.getAllRecords(model.name);
    });

    return initialState;
  },

  childContextTypes: {
    router: React.PropTypes.object.isRequired
  },

  contextTypes: {
    router: React.PropTypes.object.isRequired
  },

  componentDidMount: function () {
    models.forEach(function (model) {
      AppActions.getRecords(model);
    });

    this.context.router.listenBefore(this._clearNotifications);
    AppStore.addChangeListener(this._onChange);
    NotificationStore.addChangeListener(this._onChange);

    // activate the right tab
    var pathname = this.props.location.pathname.split('/')[1];

    if (pathname) {
      this._activate(this.refs[pathname]);
    } else {
      this._activate(this.refs.books);
    }
  },

  componentWillUnmount: function () {
    AppStore.removeChangeListener(this._onChange);
    NotificationStore.removeChangeListener(this._onChange);
  },

  _activate: function (e) {
    if (!e) return;

    var target = e.target || e;

    if (target.nodeName === 'A') {
      target = target.parentNode;
    }

    document.querySelector('.active').className = '';
    target.className = 'active';
  },

  _clearNotifications: function () {
    var notifications = this.state.notifications;
    var isStatus = notifications.status !== StatusConstants.IDLE;
    var isErrors = Object.keys(notifications.errors || {}).length;

    if (isStatus || isErrors) {
      AppActions.clearNotifications();
    }
  },

  _onChange: function () {
    var state = {
      notifications: NotificationStore.getNotifications()
    };

    models.forEach(function(model) {
      var name = model.name.toLowerCase() + 's';
      state[name] = AppStore.getAllRecords(model.name);
    });

    this.setState(state);
  },

  _renderChildren: function () {
    return React.Children.map(this.props.children, (function (child) {
      return React.cloneElement(child, this.state);
    }).bind(this));
  },

  render: function () {
    var header = models[0].name + ' Manager';

    return (
      <div>
        <div className="page-header">
          <h1><IndexLink to="/">{header}</IndexLink></h1>
          <ul className="nav nav-tabs">
            {models.map((function(model, i) {
              var refName = model.name.toLowerCase() + 's';
              var isActive = !i ? 'active' : '';
              var tabName = model.name + 's';
              var path = !i ? '/' : ('/' + refName + '/list');

              return <li ref={refName} className={isActive} onClick={this._activate}><IndexLink to={path}>{tabName}</IndexLink></li>
            }).bind(this))}
          </ul>
        </div>
        { this._renderChildren() }
      </div>
    );
  }
});

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].cl=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-71120724-1', 'auto');
ga('send', 'pageview');