var React = require('react');
var RecordListMixin = require('../mixins/RecordListMixin');
var models = require('../models/Models');

var lists = [];

models.forEach(function (model) {
  var name = model.name;
  var component = React.createClass({
    mixins: [RecordListMixin],
    model: model
  });

  lists.push({
    model: model,
    component: component
  });
});

module.exports = lists;