var React = require('react');
var CreateRecordMixin = require('../mixins/CreateRecordMixin');
var models = require('../models/Models');

var createRecords = [];

models.forEach(function (model) {
  var name = model.name;
  var component = React.createClass({
    mixins: [CreateRecordMixin],
    model: model
  });

  createRecords.push({
    model: model,
    component: component
  });
});

module.exports = createRecords;