var React = require('react');
var UpdateRecordMixin = require('../mixins/UpdateRecordMixin');
var models = require('../models/Models');

var updateRecords = [];

models.forEach(function (model) {
  var name = model.name;
  var component = React.createClass({
    mixins: [UpdateRecordMixin],
    model: model
  });

  updateRecords.push({
    model: model,
    component: component
  });
});

module.exports = updateRecords;