var React = require('react');
var RecordItemMixin = require('../mixins/RecordItemMixin');
var models = require('../models/Models');

module.exports = React.createClass({
  mixins: [RecordItemMixin]
});

// var React = require('react');
// var RecordItemMixin = require('../mixins/RecordItemMixin');
// var models = require('../models/Models');

// var records = [];

// models.forEach(function (model) {
//   var name = model.name;
//   var component = React.createClass({
//     mixins: [RecordItemMixin],
//     model: model
//   }));

//   records.push({
//     model: model,
//     component: component
//   });
// });

// module.exports = records;