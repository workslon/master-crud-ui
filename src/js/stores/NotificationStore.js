var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var StatusConstants = require('../constants/StatusConstants');
var EventEmitter = require('events').EventEmitter;

var CHANGE_EVENT = 'change';

var notifications = {
  status: StatusConstants.IDLE,
  errors: {}
};

var NotificationStore = Object.assign({}, EventEmitter.prototype, {
  getNotifications: function () {
    return notifications;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  switch(action.type) {
    // -- Create record Pending
    case AppConstants.REQUEST_RECORD_SAVE:
      notifications.status = StatusConstants.PENDING;
      NotificationStore.emitChange();
      break;

    // -- Create record Success
    case AppConstants.RECORD_SAVE_SUCCESS:
      notifications.errors = {};
      notifications.status = StatusConstants.SUCCESS;
      NotificationStore.emitChange();
      break;

    // -- Create record Error
    case AppConstants.RECORD_SAVE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      NotificationStore.emitChange();
      break;

    // -- Update record Pending
    case AppConstants.REQUEST_RECORD_UPDATE:
      notifications.status = StatusConstants.PENDING;
      NotificationStore.emitChange();
      break;

    // --- Update record Success
    case AppConstants.RECORD_UPDATE_SUCCESS:
      notifications.errors = {};
      notifications.status = StatusConstants.SUCCESS;
      NotificationStore.emitChange();
      break;

    // -- Update record Error
    case AppConstants.RECORD_UPDATE_ERROR:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = action.error;
      NotificationStore.emitChange();
      break;

    // -- Destroy record Pending
    case AppConstants.REQUEST_RECORD_DESTROY:
      notifications.status = StatusConstants.PENDING;
      NotificationStore.emitChange();
      break;

    // -- Destroy record Success
    case AppConstants.RECORD_DESTROY_SUCCESS:
      notifications.status = StatusConstants.IDLE;
      NotificationStore.emitChange();
      break;

    // --- Non-unique isbn
    case AppConstants.NON_UNIQUE:
      notifications.status = StatusConstants.ERROR;
      notifications.errors = {
        isbn: 'Non-unique!'
      };
      NotificationStore.emitChange();
      break;

    // --- Client Validation error
    case AppConstants.RECORD_VALIDATION_ERROR:
      Object.keys(action.errors).map(function (key) {
        notifications.errors[key] = action.errors[key];
      });

      notifications.status = StatusConstants.ERROR;
      NotificationStore.emitChange();
      break;

    // --- Clear all notifications (either errors or success)
    case AppConstants.CLEAR_NOTIFICATIONS:
      notifications = {
        status: StatusConstants.IDLE,
        errors: {}
      };
      NotificationStore.emitChange();
      break;
  }
});

module.exports = NotificationStore;