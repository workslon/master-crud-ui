var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppActions = require('../actions/AppActions');
var AppConstants = require('../constants/AppConstants');
var StatusConstants = require('../constants/StatusConstants');
var EventEmitter = require('events').EventEmitter;
var models = require('../models/Models');
var CHANGE_EVENT = 'change';

// generic notifications
var notifications = {
  status: StatusConstants.IDLE,
  errors: {}
};

// records (schematic)
var records = {};

models.forEach(function(model) {
  var name = model.name.toLowerCase() + 's';
  records[name] = [];
});

var AppStore = Object.assign({}, EventEmitter.prototype, {
  getRecord: function (modelName, id) {
    var name = modelName.toLowerCase() + 's';

    return records[name].filter(function (record) {
      return record.objectId === id;
    })[0] || {};
  },

  getAllRecords: function (modelName) {
    return records[modelName.toLowerCase() + 's'];
  },

  getNotifications: function () {
    return notifications;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  switch(action.type) {
    case AppConstants.REQUEST_RECORDS_SUCCESS:
      try {
        var model = action.data.model;
        var name = model.name.toLowerCase() + 's';
        records[name] = action.result;
        AppStore.emitChange();
      } catch (e) {
        console.log(e);
        alert('Invalid remote response format!');
      }

      break;

    case AppConstants.RECORD_SAVE_SUCCESS:
      try {
        var model = action.data.model;
        var name = model.name.toLowerCase() + 's';
        var data = action.data;
        var singleRefProps = Object.keys(model.properties).filter(function(prop) {
          return model.properties[prop].range === 'SingleValueRef';
        });

        data.objectId = JSON.parse(action.result).objectId;

        singleRefProps.forEach(function (prop) {
          data[prop] = data[prop] ?
                        AppStore.getRecord(model.properties[prop].className, data[prop].objectId) :
                        undefined;
        });

        records[name].push(data);

        models.forEach(function (model) {
          setTimeout(function () {
            AppActions.getRecords(model);
          }, 0);
        });

        AppStore.emitChange();
      } catch (e) {}

      break;

    case AppConstants.RECORD_UPDATE_SUCCESS:
      var model = action.data.model;
      var name = model.name.toLowerCase() + 's';
      var data = action.data;

      records[name] = records[name].map(function (record) {
        if (record.objectId === data.objectId) {
          for (var prop in model.properties) {
            if (prop.range === 'SingleValueRef') {
              record[prop] = data[prop] ?
                              AppStore['get' + model.properties[prop].className](data[prop].objectId) :
                              null;
            } else {
              record[prop] = data[prop];
            }
          }
        }
        return record;
      });

      models.forEach(function (model) {
        setTimeout(function () {
          AppActions.getRecords(model);
        }, 0);
      });

      AppStore.emitChange();
      break;

    case AppConstants.RECORD_DESTROY_SUCCESS:
      var model = action.data.model;
      var name = model.name.toLowerCase() + 's';

      records[name] = records[name].filter(function (record) {
        return record.objectId !== action.data.objectId;
      });

      models.forEach(function (model) {
        setTimeout(function () {
          AppActions.getRecords(model);
        }, 0);
      });

      AppStore.emitChange();
  }
});

module.exports = AppStore;