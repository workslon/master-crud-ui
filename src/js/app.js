window.generate = function () {
  var React = require('react');
  var ReactDOM = require('react-dom');
  var ReactRouter = require('react-router');

  var Router = ReactRouter.Router;
  var Route = ReactRouter.Route;
  var IndexRoute = ReactRouter.IndexRoute;
  var hashHistory = ReactRouter.hashHistory;

  var App = require('./components/App.react.js');
  var RecordLists = require('./components/RecordLists.react.js');
  var CreateRecords = require('./components/CreateRecords.react.js');
  var UpdateRecords = require('./components/UpdateRecords.react.js');

  var models = require('./models/Models');

  function formatName(item) {
    return item.model.name.toLowerCase() + 's';
  }

  ReactDOM.render((
    <Router history={hashHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={RecordLists[0].component} />

        {/* Lists */}
        {RecordLists.map(function (item) {
          var path = formatName(item) + '/list';
          return <Route key={item.model.name + '-list'} path={path} component={item.component} />
        })}

        {/* Create Records */}
        {CreateRecords.map(function (item) {
          var path = formatName(item) + '/create';
          return <Route key={item.model.name + '-create'} path={path} component={item.component} />
        })}

      {/* Update Records */}
        {UpdateRecords.map(function (item) {
          var path = formatName(item) + '/update/:id';
          return <Route key={item.model.name + '-update'} path={path} component={item.component} />
        })}

      </Route>
    </Router>
  ), document.getElementById('app'));
};