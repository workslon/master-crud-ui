module.exports = {
  getStdIdPropName: function (props) {
    return Object.keys(props).reduce(function (current, next) {
      if (props[current].isStandardId) {
        return current;
      } else {
        return next;
      }
    })
  }
};
