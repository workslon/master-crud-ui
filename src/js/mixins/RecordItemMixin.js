var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var Link = require('react-router/lib/Link');
var models = require('../models/Models');
var util = require('../util/util');

module.exports = {
  delete: function (e) {
    AppActions.deleteRecord(
      this.props.model,
      this.props.record
    );
  },
  render: function () {
    var model = this.props.model;
    var record = this.props.record;
    var modelProps = model.properties;
    var updatePath = model.name.toLowerCase() + 's/update/' + record.objectId;

    return (
      <tr>
        <td scope="row">{this.props.nr}</td>
        {Object.keys(modelProps).map(function (prop) {
          if (modelProps[prop].range === 'SingleValueRef') {
            var propModel = models.filter(function (model) {
              return model.name.toLowerCase() === prop;
            })[0];
            var props = propModel.properties;
            var stdIdPropName = util.getStdIdPropName(props);
            var name = (record[prop] && (record[prop].name || record[prop].title)) || '';
            name && (name += ' - ');

            return <td key={prop}>{record[prop] ? (name + record[prop][stdIdPropName]) : ''}</td>
          } else if (modelProps[prop].range === 'MultiValueRef') {

            return (
              <td>
                <ul>
                  {record[prop] && record[prop].map(function (record) {
                    var propModel = models.filter(function (model) {
                      return model.name === modelProps[prop].className;
                    })[0];
                    var props = propModel.properties;
                    var stdIdPropName = util.getStdIdPropName(props);

                    return <li>{record[stdIdPropName]}</li>
                  })}
                </ul>
              </td>
            )
          } else {
            if (modelProps[prop].range instanceof eNUMERATION) {
              if (!record[prop]) {
                return <td key={record[prop]}></td>
              } else {
                return <td key={record[prop]}>{modelProps[prop].range.enumLitNames[record[prop] - 1]}</td>
              }
            } else {
              return <td key={prop}>{record[prop]}</td>
            }
          }

        })}
        <td className="action-links">
          <Link className="btn btn-primary btn-xs" to={updatePath}>Update</Link>
          <a className="btn btn-danger btn-xs" onClick={this.delete}>Delete</a>
        </td>
      </tr>
    );
  }
};