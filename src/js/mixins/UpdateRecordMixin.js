var React = require('react');
var ReactDOM = require('react-dom');
var AppStore = require('../stores/AppStore');
var AppActions = require('../actions/AppActions');
var StatusConstants = require('../constants/StatusConstants');
var IndexLink = require('react-router/lib/IndexLink');
var models = require('../models/Models');
var util = require('../util/util');

module.exports = {
  displayName: 'UpdateRecord',

  componentWillMount: function () {
    this.record = AppStore.getRecord(this.model.name, this.props.params.id);
    AppActions.getRecords(this.model);
  },

  componentWillUpdate: function () {
    this.record = AppStore.getRecord(this.model.name, this.props.params.id);
  },

  update: function (e) {
    e.preventDefault();

    var modelProps = this.model.properties;
    var refs = this.refs; // note, "stdId" field doesn't have "ref"
    var fields = {};

    for (var key in refs) {
      if (modelProps[key].range && (modelProps[key].range === 'Integer' || (modelProps[key].range instanceof eNUMERATION))) {
        fields[key] = parseInt(refs[key].value);
      } else if (modelProps[key].range === 'SingleValueRef') {
        fields[key] = {
          objectId: refs[key].value
        }
      } else {
        fields[key] = refs[key].value;
      }
    }

    AppActions.updateRecord(this.model, this.record, fields);
  },

  validate: function (e) {
    AppActions.validate(
      this.model,
      e.target.id,
      e.target.value
    );
  },

  render: function () {
    var model = this.model;
    var record = this.record;
    var modelProps = model.properties;
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    var singleRefProps = Object.keys(modelProps).filter(function(prop) {
      return modelProps[prop].range === 'SingleValueRef';
    }).map(function(prop) {
      return {
        model: models.filter(function(model) {
          return model.name === modelProps[prop].className
        })[0],
        records: AppStore.getAllRecords(modelProps[prop].className)
      };
    });

    return (
      <form>
        <h3>Update {model.name}</h3>
        {Object.keys(modelProps).map((function (prop) {
          if (modelProps[prop].range !== 'SingleValueRef' && modelProps[prop].range !== 'MultiValueRef') {
            if (modelProps[prop].range && modelProps[prop].range instanceof eNUMERATION) {
              return (
                <div key={prop} className="form-group">
                  <label forHtml={prop}>{modelProps[prop].label}</label>
                  <select defaultValue={record[prop]} className="form-control" ref={prop} id={prop}>
                    <option key={prop} value="0">-select {prop}-</option>
                    {modelProps[prop].range.labels.map(function (label) {
                      return <option key={label} value={modelProps[prop].range.labels.indexOf(label) + 1}>{label}</option>
                    })}
                  </select>
                </div>
              );
            } else {
              return (
                <div key={prop} className="form-group">
                  <label forHtml={prop}>{modelProps[prop].label}</label>
                  {modelProps[prop].isStandardId ?
                    <span>{record[prop]}</span> :
                    <input defaultValue={record[prop]} onChange={this.validate} ref={prop} type="text" className="form-control" id={prop} placeholder={modelProps[prop].label} />}
                    {errors[prop] && <span className="text-danger">{errors[prop]}</span>}
                </div>
              );
            }
          }
        }).bind(this))}

        {singleRefProps.map(function(prop) {
          var lowLabel = prop.model.name.toLowerCase();

          return (
            <div key={lowLabel} className="form-group">
              <label forHtml={lowLabel}>{prop.model.name}</label>
              <select defaultValue={(record[lowLabel] || {}).objectId} className="form-control" ref={lowLabel} id={lowLabel}>
                <option key={lowLabel} value="">-select {lowLabel}-</option>
                {prop.records.map(function(record) {
                  var props = prop.model.properties;
                  var stdIdPropName = util.getStdIdPropName(props);
                  var name = record.name || record.title || '';
                  name && (name += ' - ');

                  return <option key={record.objectId} value={record.objectId}>{name + record[stdIdPropName]}</option>
                })}
              </select>
            </div>
          )
        })}
        <button type="submit" onClick={this.update} className="btn btn-default">Submit</button>
        {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
        {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
        <IndexLink className="back" to={model.name.toLowerCase() + 's/list'}>&laquo; back</IndexLink>
      </form>
    );
  }
};