var React = require('react');
var StatusConstants = require('../constants/StatusConstants');
var Link = require('../../../node_modules/react-router/lib/Link');
var RecordItem = require('../components/RecordItem.react');
var util = require('../util/util');

module.exports = {
  render: function () {
    var model = this.model;
    var modelProps = model.properties;
    var stdIdPropName = util.getStdIdPropName(modelProps);
    var notifications = this.props.notifications || {};
    var status = notifications.status;
    var records = this.props[model.name.toLowerCase() + 's'];
    var path = model.name.toLowerCase() + 's/create';

    return (
      <div>
        <Link className="create-book btn btn-success bt-sm" to={path}>+ Add {model.name}</Link>
        {status === StatusConstants.PENDING && <p className="bg-info">Deleting...</p>}
        {records.length ?
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                {Object.keys(modelProps).map(function (prop) {
                  return <th key={prop}>{modelProps[prop].label}</th>
                })}
                <th></th>
              </tr>
            </thead>
            <tbody>
              {records.map((function (record, i) {
                return (
                  <RecordItem key={record.objectId} nr={i + 1} record={record} model={model} />
                );
              }).bind(this))}
            </tbody>
          </table>
          : <div>Loading records...</div>}
      </div>
    );
  }
};