window.modelClasses = [
  new mODELcLASS({
    name: 'Book',
    properties: {
      isbn: {
        label: 'ISBN',
        range: 'NonEmptyString',
        isStandardId: true,
        pattern: /\b\d{9}(\d|X)\b/,
        patternMessage: 'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!'
      },

      title: {
        label: 'Title',
        range: 'NonEmptyString',
        min: 2,
        max: 50
      },

      year: {
        label: 'Year',
        range: 'Integer',
        min: 1459,
        max: (new Date()).getFullYear()
      },

      genre: {
        label: 'Genre',
        range: new eNUMERATION('Genre', ['Drama', 'Comedy']),
        optional: true
      },

      publisher: {
        className: 'Publisher',
        label: 'Publisher',
        range: 'SingleValueRef',
        inverseRef: {
          name: 'publishedBooks',
          range: 'MultiValueRef'
        },
        optional: true
      }
    }
  }),

  new mODELcLASS({
    name: 'Publisher',
    properties: {
      name: {
        label: 'Name',
        range: 'NonEmptyString',
        min: 2,
        max: 50
      },

      email: {
        label: 'Email',
        range: 'NonEmptyString',
        isStandardId: true,
        pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        patternMessage: 'You entered invalid email!'
      },

      publishedBooks: {
        label: 'Published Books',
        className: 'Book',
        range: 'MultiValueRef',
        optional: true
      }
    }
  })
];