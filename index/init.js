var codemirror = CodeMirror.fromTextArea(document.querySelector('#codemirror textarea'), {
  mode: 'javascript',
  indentUnit: 2,
  indentWithTabs: true,
  smartIndent: false,
  lineNumbers: true,
  matchBrackets: true,
  autoCloseBrackets: true,
  showTrailingSpace: true,
  readOnly: false,
  gutters: ['CodeMirror-lint-markers'],
});

document.querySelector('#btn-generate').addEventListener('click', function (e) {
  e.preventDefault();

  // get modelClasses
  window.modelClasses = eval('window.modelClasses=' + codemirror.getValue());
  // hide code editor
  document.querySelector('body > .page-header').style.display = 'none';
  // show "regenerate" button
  document.querySelector('#btn-regenerate').style.display = 'block';
  // generate UI
  window.generate();
}, false);

document.querySelector('#btn-regenerate').addEventListener('click', function (e) {
  window.location.hash = '/';
  window.location.reload();
});