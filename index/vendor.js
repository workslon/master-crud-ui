"use strict";
function ConstraintViolation(e, u) {
    this.message = e,
    u && (this.culprit = u)
}
function NoConstraintViolation(e) {
    this.checkedValue = e,
    this.message = ""
}
function MandatoryValueConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function RangeConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function StringLengthConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function IntervalConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function PatternConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function UniquenessConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function CardinalityConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function EntityTypeConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function ModelClassConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function ViewConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function ObjectTypeConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function AgentTypeConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function KindConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function RoleConstraintViolation(e, u) {
    ConstraintViolation.call(this, e, u)
}
function eNUMERATION(e, u) {
    var t = 0
      , a = ""
      , n = "";
    if ("string" != typeof e)
        throw new ConstraintViolation("The first constructor argument of an enumeration must be a string!");
    if (Array.isArray(u)) {
        if (!u.every(function(e) {
            return "string" == typeof e
        }))
            throw new ConstraintViolation("A list of enumeration labels as the second constructor argument must be an array of strings!");
        Object.defineProperties(this, {
            name: {
                value: e,
                enumerable: false,
                writable: true
            },
            labels: {
                value: u,
                enumerable: false,
                writable: true
            },
            enumLitNames: {
                value: null,
                enumerable: false,
                writable: true
            },
            codeList: {
                value: null,
                enumerable: false,
                writable: true
            },
            MAX: {
              value: null,
              enumerable: false,
              writable: true
            }
        });

        this.enumLitNames = this.labels
    } else {
        if (!("object" == typeof u && Object.keys(u).length > 0))
            throw new ConstraintViolation("Invalid Enumeration constructor argument: " + u);
        if (!Object.keys(u).every(function(e) {
            return "string" == typeof u[e]
        }))
            throw new ConstraintViolation("All values of a code list map must be strings!");
        this.codeList = u,
        this.enumLitNames = Object.keys(this.codeList),
        this.labels = this.enumLitNames.map(function(e) {
            return u[e] + " (" + e + ")"
        })
    }
    for (this.MAX = this.enumLitNames.length,
    t = 1; t <= this.enumLitNames.length; t++)
        a = this.enumLitNames[t - 1].replace(/( |-)/g, "_"),
        n = a.split("_").map(function(e) {
            return e.toUpperCase()
        }).join("_"),
        this[n] = t;
    Object.freeze(this);
    eNUMERATION.instances[this.name] = this
}
function eNTITYtYPE(e) {
    var u = 0
      , t = null
      , a = {
        properties: {},
        methods: {},
        staticMethods: {},
        reactionRules: {},
        internalEventTypes: {}
    };
    if (this.name = e.name,
    e.storageAdapter ? this.storageAdapter = e.storageAdapter : e.isNonPersistent && (this.isNonPersistent = e.isNonPersistent),
    Object.keys(e.properties).forEach(function(u) {
        var t = e.properties[u]
          , n = t.minCard
          , o = t.maxCard;
        if (!(t.range || e.methods && "validate" in e.methods))
            throw new EntityTypeConstraintViolation("Either there must be a range definition for " + u + " or its's range must be checked in a 'validate' method!");
        if (void 0 !== n) {
            if (!Number.isInteger(n) || 0 > n)
                throw new EntityTypeConstraintViolation("minCard value for " + u + " (" + JSON.stringify(o)) + ") is invalid (not a non-negative integer)!";
            if (void 0 === o)
                throw new EntityTypeConstraintViolation("A minCard value for " + u + " is only meaningful when maxCard is defined!");
            if ((!Number.isInteger(o) || 2 > o) && o !== 1 / 0)
                throw new EntityTypeConstraintViolation("Invalid maxCard value for " + u + ": " + JSON.stringify(o));
            if (n > o)
                throw new EntityTypeConstraintViolation("maxCard value for " + u + " is less than mincCard value! ")
        }
        (t.isStandardId || "id" === u) && (this.standardId = u),
        a.properties[u] = util.mergeObjects({
            writable: !0,
            enumerable: !0
        }, t)
    }, this),
    e.properties = a.properties,
    e.supertype || e.supertypes)
        if (e.supertype)
            this.supertype = e.supertype,
            this.standardId = e.supertype.standardId,
            Object.keys(a).forEach(function(u) {
                (e[u] || e.supertype[u]) && (this[u] = util.mergeObjects(e.supertype[u], e[u]))
            }, this);
        else {
            for (this.supertypes = e.supertypes,
            u = 0; u < e.supertypes.length; u++)
                t = e.supertypes[u],
                Object.keys(a).forEach(function(e) {
                    t[e] && (a[e] = util.mergeObjects(a[e], t[e]))
                });
            Object.keys(a).forEach(function(u) {
                (a[u] || e[u]) && (this[u] = util.mergeObjects(a[u], e[u]))
            }, this)
        }
    else
        Object.keys(a).forEach(function(u) {
            e[u] && (this[u] = e[u])
        }, this),
        this.methods || (this.methods = {}),
        this.methods.toString = function() {
            var e = this.type.name + "{ ";
            return Object.keys(this).forEach(function(u) {
                "type" !== u && void 0 !== this[u] && (e += u + ": " + this[u] + ", ")
            }, this),
            e + "}"
        }
        ,
        this.methods.set = function(e, u) {
            var t = this.type.check(e, u);
            if (!(t instanceof NoConstraintViolation))
                throw t;
            this[e] = t.checkedValue
        }
        ,
        this.methods.isInstanceOf = function(e) {
            return this.type ? this.type === e ? !0 : this.type.isSubTypeOf(e) : !1
        }
        ,
        this.methods.toDisplayString = function(e) {
            var u = this
              , t = []
              , a = ""
              , n = 0
              , o = ", "
              , i = u.type.properties[e]
              , r = i.range
              , s = u[e];
            if (void 0 === s)
                return "";
            if (i.maxCard && i.maxCard > 1 ? r instanceof mODELcLASS ? t = Array.isArray(s) ? s.slice(0) : Object.values(s) : Array.isArray(s) ? t = s.slice(0) : console.log("Invalid non-array collection in to Record!") : t = [s],
            t.forEach(function(e, u) {
                r instanceof eNUMERATION ? t[u] = r.labels[e - 1] : ["number", "string", "boolean"].includes(typeof e) || !e ? t[u] = String(e) : "Date" === r ? t[u] = util.createIsoDateString(e) : r instanceof mODELcLASS ? t[u] = e[r.standardId] : Array.isArray(e) ? t[u] = e.slice(0) : t[u] = JSON.stringify(e)
            }),
            a = t[0],
            i.maxCard && i.maxCard > 1)
                for (n = 1; n < t.length; n++)
                    a += o + t[n];
            return a
        }
        ,
        this.methods.toRecord = function() {
            var e, u, t = this, a = {}, n = {}, o = [];
            return Object.keys(t).forEach(function(i) {
                "type" !== i && void 0 !== t[i] && (u = t[i],
                n = t.type.properties[i],
                e = n.range,
                n.maxCard && n.maxCard > 1 ? e instanceof mODELcLASS ? o = Array.isArray(u) ? u.slice(0) : Object.values(u) : Array.isArray(u) ? o = u.slice(0) : console.log("Invalid non-array collection in to Record!") : o = [u],
                o.forEach(function(u, t) {
                    ["number", "string", "boolean"].includes(typeof u) || !u ? o[t] = String(u) : "Date" === e ? o[t] = util.createIsoDateString(u) : e instanceof mODELcLASS ? o[t] = u[e.standardId] : Array.isArray(u) ? o[t] = u.slice(0) : o[t] = JSON.stringify(u)
                }),
                !n.maxCard || n.maxCard <= 1 ? a[i] = o[0] : a[i] = o)
            }),
            a
        }
}
function mODELcLASS(e) {
    var u = {};
    if (e.supertype && !(e.supertype instanceof mODELcLASS))
        throw new ModelClassConstraintViolation("Supertype " + e.supertype + " of mODELcLASS " + this.name + " is not a mODELcLASS!");
    if (eNTITYtYPE.call(this, e),
    !(this.supertype || this.supertypes || this.isNonPersistent || this.standardId))
        throw new ModelClassConstraintViolation("Model class " + this.name + " must define a standard identifier attribute!");
    if (u = this.properties,
    e.attributesToDisplayInLists) {
        if (!e.attributesToDisplayInLists.every(function(e) {
            return u[e]
        }))
            throw new ModelClassConstraintViolation("There is an attribute to display in lists, which is not a property of mODELcLASS " + this.name + " !");
        this.attributesToDisplayInLists = e.attributesToDisplayInLists
    }
    this.instances = {},
    mODELcLASS.classes[this.name] = this
}
function sTORAGEmANAGER(e) {
    if (e) {
        if ("object" != typeof e || void 0 === e.name || !["LocalStorage", "ParseRestAPI", "MySQL"].includes(e.name))
            throw new ConstraintViolation("Invalid value for adapter record");
        this.adapter = e
    } else
        this.adapter = {
            name: "LocalStorage"
        };
    sTORAGEmANAGER.adapters[this.adapter.name].currentAdapter = e
}
String.prototype.trim || (String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "")
}
),
Number.isInteger || (Number.isInteger = function(e) {
    return "number" == typeof e && isFinite(e) && e > -9007199254740992 && 9007199254740992 > e && Math.floor(e) === e
}
),
String.prototype.includes || (String.prototype.includes = function() {
    return -1 !== String.prototype.indexOf.apply(this, arguments)
}
),
Array.prototype.includes || (Array.prototype.includes = function() {
    return -1 !== Array.prototype.indexOf.apply(this, arguments)
}
),
Array.max = function(e) {
    return Math.max.apply(Math, e)
}
,
Array.min = function(e) {
    return Math.min.apply(Math, e)
}
,
Array.prototype.clone = function() {
    return this.slice(0)
}
,
Array.prototype.isEqualTo = function(e) {
    return this.length === e.length && this.every(function(u, t) {
        return u === e[t]
    })
}
,
Object.values = function(e) {
    return Object.keys(e).map(function(u) {
        return e[u]
    })
}
;
var util = {
    languages: {
        de: "Deutsch",
        en: "English",
        es: "Español",
        fr: "Français",
        pt: "Português",
        ru: "Русский",
        zh: "中文",
        ro: "Romanian"
    },
    identifierPattern: /^(?!(?:do|if|in|for|let|new|try|var|case|else|enum|eval|false|null|this|true|void|with|break|catch|class|const|super|throw|while|yield|delete|export|import|public|return|static|switch|typeof|default|extends|finally|package|private|continue|debugger|function|arguments|interface|protected|implements|instanceof)$)[$A-Z\_a-z\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc][$A-Z\_a-z\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc0-9\u0300-\u036f\u0483-\u0487\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u0610-\u061a\u064b-\u0669\u0670\u06d6-\u06dc\u06df-\u06e4\u06e7\u06e8\u06ea-\u06ed\u06f0-\u06f9\u0711\u0730-\u074a\u07a6-\u07b0\u07c0-\u07c9\u07eb-\u07f3\u0816-\u0819\u081b-\u0823\u0825-\u0827\u0829-\u082d\u0859-\u085b\u08e4-\u08fe\u0900-\u0903\u093a-\u093c\u093e-\u094f\u0951-\u0957\u0962\u0963\u0966-\u096f\u0981-\u0983\u09bc\u09be-\u09c4\u09c7\u09c8\u09cb-\u09cd\u09d7\u09e2\u09e3\u09e6-\u09ef\u0a01-\u0a03\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a66-\u0a71\u0a75\u0a81-\u0a83\u0abc\u0abe-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ae2\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b3c\u0b3e-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b62\u0b63\u0b66-\u0b6f\u0b82\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd7\u0be6-\u0bef\u0c01-\u0c03\u0c3e-\u0c44\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c62\u0c63\u0c66-\u0c6f\u0c82\u0c83\u0cbc\u0cbe-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0ce2\u0ce3\u0ce6-\u0cef\u0d02\u0d03\u0d3e-\u0d44\u0d46-\u0d48\u0d4a-\u0d4d\u0d57\u0d62\u0d63\u0d66-\u0d6f\u0d82\u0d83\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e31\u0e34-\u0e3a\u0e47-\u0e4e\u0e50-\u0e59\u0eb1\u0eb4-\u0eb9\u0ebb\u0ebc\u0ec8-\u0ecd\u0ed0-\u0ed9\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f3e\u0f3f\u0f71-\u0f84\u0f86\u0f87\u0f8d-\u0f97\u0f99-\u0fbc\u0fc6\u102b-\u103e\u1040-\u1049\u1056-\u1059\u105e-\u1060\u1062-\u1064\u1067-\u106d\u1071-\u1074\u1082-\u108d\u108f-\u109d\u135d-\u135f\u1712-\u1714\u1732-\u1734\u1752\u1753\u1772\u1773\u17b4-\u17d3\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u18a9\u1920-\u192b\u1930-\u193b\u1946-\u194f\u19b0-\u19c0\u19c8\u19c9\u19d0-\u19d9\u1a17-\u1a1b\u1a55-\u1a5e\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1b00-\u1b04\u1b34-\u1b44\u1b50-\u1b59\u1b6b-\u1b73\u1b80-\u1b82\u1ba1-\u1bad\u1bb0-\u1bb9\u1be6-\u1bf3\u1c24-\u1c37\u1c40-\u1c49\u1c50-\u1c59\u1cd0-\u1cd2\u1cd4-\u1ce8\u1ced\u1cf2-\u1cf4\u1dc0-\u1de6\u1dfc-\u1dff\u200c\u200d\u203f\u2040\u2054\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2cef-\u2cf1\u2d7f\u2de0-\u2dff\u302a-\u302f\u3099\u309a\ua620-\ua629\ua66f\ua674-\ua67d\ua69f\ua6f0\ua6f1\ua802\ua806\ua80b\ua823-\ua827\ua880\ua881\ua8b4-\ua8c4\ua8d0-\ua8d9\ua8e0-\ua8f1\ua900-\ua909\ua926-\ua92d\ua947-\ua953\ua980-\ua983\ua9b3-\ua9c0\ua9d0-\ua9d9\uaa29-\uaa36\uaa43\uaa4c\uaa4d\uaa50-\uaa59\uaa7b\uaab0\uaab2-\uaab4\uaab7\uaab8\uaabe\uaabf\uaac1\uaaeb-\uaaef\uaaf5\uaaf6\uabe3-\uabea\uabec\uabed\uabf0-\uabf9\ufb1e\ufe00-\ufe0f\ufe20-\ufe26\ufe33\ufe34\ufe4d-\ufe4f\uff10-\uff19\uff3f]*$/,
    randomInt: function(e, u) {
        return Math.floor(Math.random() * (u - e + 1)) + e
    },
    getUserLanguage: function() {
        var e = window.navigator.userLanguage || window.navigator.language;
        return e.substring(0, 2)
    },
    isNonEmptyString: function(e) {
        return "string" == typeof e && "" !== e.trim()
    },
    isIntegerString: function(e) {
        return "string" == typeof e && 0 === e.search(/^-?[0-9]+$/)
    }
};
util.createIsoDateString = function(e) {
    return e.toISOString().substring(0, 10)
}
,
util.nextYear = function() {
    var e = new Date;
    return e.getFullYear() + 1
}
,
util.getAge = function(e) {
    var u, t, a = new Date;
    return "string" == typeof e && (e = new Date(e)),
    u = a.getFullYear() - e.getFullYear(),
    t = a.getMonth() - e.getMonth(),
    (0 > t || 0 === t && a.getDate() < e.getDate()) && u--,
    u
}
,
util.getTableName = function(e) {
    return util.camelToUnderscore(e, !0)
}
,
util.getColumnName = function(e) {
    return util.camelToUnderscore(e)
}
,
util.getClassName = function(e) {
    return util.underscoreToCamel(e, !0)
}
,
util.getPropertyName = function(e) {
    return util.underscoreToCamel(e)
}
,
util.camelToUnderscore = function(e, u) {
    var t = "";
    if (!e)
        throw "util.camelToUnderscore: the 'identifier' can't be null or undefined!";
    if (!util.identifierPattern.test(e))
        throw "util.camelToUnderscore: the provided 'identifier' (" + e + ") is not a JS valid identifier!";
    return e = e.charAt(0).toLowerCase() + e.slice(1),
    t = e.replace(/([A-Z])/g, function(e) {
        return "_" + e.toLowerCase()
    }),
    u === !0 && (t += "s"),
    t
}
,
util.underscoreToCamel = function(e, u) {
    var t = "";
    if (!e)
        throw "util.underscoreToCamel: the 'identifier' can't be null or undefined!";
    if (!util.identifierPattern.test(e))
        throw "util.underscoreToCamel: the provided 'identifier' is not a JS valid identifier!";
    return t = e.replace(/(\_[a-z])/g, function(e) {
        return e.toUpperCase().replace("_", "")
    }),
    u === !0 && (t = t.charAt(0).toUpperCase() + t.slice(1),
    "s" === t.charAt(t.length - 1) && (t = t.slice(0, t.length - 1))),
    t
}
,
util.createRecordFromObject = function(e) {
    var u, t = {}, a = "";
    for (a in e)
        u = e[a],
        e.hasOwnProperty(a) && ("string" == typeof u || "number" == typeof u || "boolean" == typeof u || u instanceof Date || Array.isArray(u) && !u.some(function(e) {
            return "object" == typeof e
        })) && (u instanceof Date ? t[a] = u.toISOString() : Array.isArray(u) ? t[a] = u.slice(0) : t[a] = u);
    return t
}
,
util.cloneObject = function(e) {
    var u, t = "", a = Object.create(Object.getPrototypeOf(e));
    for (t in e)
        e.hasOwnProperty(t) && (u = e[t],
        ("number" == typeof u || "string" == typeof u || "boolean" == typeof u || u instanceof Date || "object" == typeof u && u.constructor || Array.isArray(u) && !u.some(function(e) {
            return "object" == typeof e
        }) || Array.isArray(u) && u.every(function(e) {
            return "object" == typeof e && !!e.constructor
        })) && (Array.isArray(u) ? a[t] = u.slice(0) : a[t] = u));
    return a
}
,
util.mergeObjects = function() {
    var e = 0
      , u = 0
      , t = null
      , a = {}
      , n = []
      , o = "";
    for (e = 0; e < arguments.length; e++)
        if (t = arguments[e],
        t && "object" == typeof t)
            for (n = Object.keys(t),
            u = 0; u < n.length; u++)
                o = n[u],
                a[o] = t[o];
    return a
}
;
var dom = {
    element: function(e, u) {
        var t = document.createElement(e);
        return u && (u.id && (t.id = u.id),
        u.classValues && (t.className = u.classValues),
        u.content && (t.innerHTML = u.content)),
        t
    },
    createTime: function(e) {
        var u = document.createElement("time");
        return u.textContent = e.toLocaleDateString(),
        u.setAttribute("datetime", e.toISOString()),
        u
    },
    createImg: function(e) {
        var u = document.createElement("img");
        return u.src = e.src,
        e.id && (u.id = e.id),
        e.classValues && (u.className = e.classValues),
        u
    },
    createStringInput: function(e) {
        var u = document.createElement("input");
        return e.id && (u.id = e.id),
        e.classValues && (u.className = e.classValues),
        e.name && (u.name = e.name),
        u
    },
    createNumInput: function(e) {
        var u = dom.createStringInput(e);
        return u.type = "number",
        u
    },
    createOption: function(e) {
        var u = document.createElement("option");
        return e.text && (u.textContent = e.text),
        e.value && (u.value = e.value),
        u
    },
    createButton: function(e) {
        var u = document.createElement("button");
        return u.type = "button",
        e.id && (u.id = e.id),
        e.classValues && (u.className = e.classValues),
        e.content && (u.innerHTML = e.content),
        u
    },
    createMenuItem: function(e) {
        var u = document.createElement("li")
          , t = document.createElement("button");
        return t.type = "button",
        e.id && (u.id = e.id),
        e.classValues && (u.className = e.classValues),
        e.content && (t.innerHTML = e.content),
        u.appendChild(t),
        u
    },
    createLabeledInputField: function(e) {
        var u = document.createElement("input")
          , t = document.createElement("label");
        return e.name && (u.name = e.name),
        e.type && (u.type = e.type),
        void 0 !== e.value && (u.value = e.value),
        e.disabled && (u.disabled = "disabled"),
        t.textContent = e.labelText,
        t.appendChild(u),
        t
    },
    createLabeledChoiceControl: function(e, u, t, a) {
        var n = document.createElement("input")
          , o = document.createElement("label");
        return n.type = e,
        n.name = u,
        n.value = t,
        o.appendChild(n),
        o.appendChild(document.createTextNode(a)),
        o
    },
    createLabeledSelectField: function(e) {
        var u = document.createElement("select")
          , t = document.createElement("label")
          , a = document.createElement("div");
        return e.name && (u.name = e.name),
        void 0 !== e.index && (u.index = e.index),
        t.textContent = e.labelText,
        e.classValues && (a.className = e.classValues),
        t.appendChild(u),
        a.appendChild(t),
        a
    },
    fillSelectWithOptions: function(e, u) {
        e.innerHTML = "",
        e.add(dom.createOption({
            text: " --- ",
            value: ""
        }), null ),
        u.forEach(function(u, t) {
            e.add(dom.createOption({
                text: u,
                value: t
            }), null )
        })
    },
    fillSelectWithOptionsFromObjectMap: function(e, u, t, a) {
        var n = null
          , o = []
          , i = null
          , r = 0;
        for (e.innerHTML = "",
        e.appendChild(dom.createOption({
            text: " --- ",
            value: ""
        })),
        o = Object.keys(u),
        r = 0; r < o.length; r++)
            i = u[o[r]],
            n = document.createElement("option"),
            n.value = i[t],
            n.text = i[t],
            a && a.forEach(function(e) {
                n.text = n.text + " / " + i[e]
            }),
            e.add(n, null )
    },
    createBackButton: function(e) {
        var u = document.createElement("button")
          , t = document.createElement("div");
        return u.type = "button",
        u.name = "backButton",
        e && e.label ? u.textContent = e.label : u.textContent = "Back to menu",
        e && (e.classValues && (t.className = e.classValues),
        e.handler && u.addEventListener("click", e.handler)),
        t.appendChild(u),
        t
    },
    createCommitAndBackButtons: function(e) {
        var u = document.createElement("button")
          , t = document.createElement("button")
          , a = document.createElement("div");
        return e && e.label ? u.textContent = e.label : u.textContent = "Submit",
        u.type = "submit",
        u.name = "submitButton",
        t.textContent = "Back to menu",
        t.type = "button",
        t.name = "backButton",
        e && e.classValues && (a.className = e.classValues),
        a.appendChild(u),
        a.appendChild(t),
        a
    },
    createTable: function(e) {
        var u = document.createElement("table")
          , t = null ;
        return e && e.classValues && (u.className = e.classValues),
        t = document.createElement("thead"),
        u.appendChild(t),
        t = document.createElement("tbody"),
        u.appendChild(t),
        u
    }
}
  , xhr = {
    OPTIONS: function(e) {
        e.method = "OPTIONS",
        xhr.makeRequest(e)
    },
    makeOptionsRequest: this.OPTIONS,
    GET: function(e) {
        e.method = "GET",
        xhr.makeRequest(e)
    },
    makeGetRequest: this.GET,
    POST: function(e) {
        e.method = "POST",
        xhr.makeRequest(e)
    },
    makePostRequest: this.POST,
    PUT: function(e) {
        e.method = "PUT",
        xhr.makeRequest(e)
    },
    makePutRequest: this.PUT,
    DELETE: function(e) {
        e.method = "DELETE",
        xhr.makeRequest(e)
    },
    makeDeleteRequest: this.DELETE,
    makeRequest: function(e) {
        var u = null
          , t = ""
          , a = ""
          , n = ""
          , o = ""
          , i = null
          , r = /\b(https?):\/\/[\-A-Za-z0-9+&@#\/%?=~_|!:,.;]*[\-A-Za-z0-9+&@#\/%=~_|??]/;
        return e.url ? r.test(e.url) ? (t = e.url,
        u = new XMLHttpRequest,
        a = e.method ? e.method : "GET",
        n = e.reqFormat ? e.reqFormat : "application/x-www-form-urlencoded",
        o = e.respFormat ? e.respFormat : "application/json",
        i = e.handleResponse ? e.handleResponse : function(e) {
            200 === e.status || 201 === e.status || 304 === e.status ? console.log("Response: " + e.responseText) : console.log("Error " + e.status + ": " + e.statusText)
        }
        ,
        u.open(a, t, !0),
        u.onreadystatechange = function() {
            4 === u.readyState && i(u)
        }
        ,
        e.requestHeaders && Object.keys(e.requestHeaders).forEach(function(t) {
            u.setRequestHeader(t, e.requestHeaders[t])
        }),
        u.setRequestHeader("Accept", o),
        void ("GET" === a || "DELETE" === a ? u.send("") : (u.setRequestHeader("Content-Type", n),
        u.send(e.msgBody)))) : void console.log("Invalid URL in XHR GET request!") : void console.log("Missing value for url parameter in XHR GET request!")
    }
};
NoConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
NoConstraintViolation.prototype.constructor = NoConstraintViolation,
MandatoryValueConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
MandatoryValueConstraintViolation.prototype.constructor = MandatoryValueConstraintViolation,
RangeConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
RangeConstraintViolation.prototype.constructor = RangeConstraintViolation,
StringLengthConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
StringLengthConstraintViolation.prototype.constructor = StringLengthConstraintViolation,
IntervalConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
IntervalConstraintViolation.prototype.constructor = IntervalConstraintViolation,
PatternConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
PatternConstraintViolation.prototype.constructor = PatternConstraintViolation,
UniquenessConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
UniquenessConstraintViolation.prototype.constructor = UniquenessConstraintViolation,
CardinalityConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
CardinalityConstraintViolation.prototype.constructor = CardinalityConstraintViolation,
EntityTypeConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
EntityTypeConstraintViolation.prototype.constructor = EntityTypeConstraintViolation,
ModelClassConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
ModelClassConstraintViolation.prototype.constructor = ModelClassConstraintViolation,
ViewConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
ViewConstraintViolation.prototype.constructor = ViewConstraintViolation,
ObjectTypeConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
ObjectTypeConstraintViolation.prototype.constructor = ObjectTypeConstraintViolation,
AgentTypeConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
AgentTypeConstraintViolation.prototype.constructor = AgentTypeConstraintViolation,
KindConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
KindConstraintViolation.prototype.constructor = KindConstraintViolation,
RoleConstraintViolation.prototype = Object.create(ConstraintViolation.prototype),
RoleConstraintViolation.prototype.constructor = RoleConstraintViolation,
eNUMERATION.prototype.isValidEnumLitOrIndex = function(e) {
    return Number.isInteger(e) && e > 0 && e < this.MAX
}
,
eNUMERATION.prototype.enumIndexesToNames = function(e) {
    var u = e.map(function(e) {
        return this.enumLitNames[e - 1]
    }, this).join(", ");
    return u
}
,
eNUMERATION.instances = {},
eNTITYtYPE.prototype.isSubTypeOf = function(e) {
    return this.supertype || this.supertypes ? this.supertype ? this.supertype === e ? !0 : this.supertype.isSubTypeOf(e) : this.supertypes.indexOf(e) > -1 ? !0 : this.supertypes.some(function(u) {
        return u.isSubTypeOf(e)
    }) : !1
}
,
eNTITYtYPE.isIntegerType = function(e) {
    return "string" == typeof e && ["Integer", "NonNegativeInteger", "PositiveInteger"].indexOf(e) > -1 || e instanceof eNUMERATION
}
,
eNTITYtYPE.toFormFieldValue = function(e, u, t) {
    var a = e.properties[u]
      , n = a.range;
    return void 0 === t ? "" : n instanceof eNUMERATION ? n.labels[t - 1] : ["number", "string", "boolean"].includes(typeof t) ? String(t) : "Date" === n ? util.createIsoDateString(t) : n instanceof mODELcLASS ? t[n.standardId] : JSON.stringify(t)
}
,
eNTITYtYPE.prototype.check = function(e, u) {
    var t, a, n = this.properties[e], o = null , i = [], r = 0, s = [], c = "", f = "", d = 0, l = 1, b = 0, p = "";
    if (!n)
        return new EntityTypeConstraintViolation("Property to be checked (" + e + ") is not among the type's properties: " + Object.keys(this.properties).toString());
    if (a = n.range,
    b = n.min || b,
    t = n.max,
    d = n.minCard || d,
    l = n.maxCard || l,
    p = n.pattern,
    c = n.patternMessage,
    f = n.label || e,
    void 0 === u || "" === u)
        return n.optional ? new NoConstraintViolation : new MandatoryValueConstraintViolation("A value for " + e + " is required!");
    if (1 === l)
        i = [u];
    else if (Array.isArray(u))
        i = u;
    else if ("object" == typeof u && Object.keys(u).length > 0)
        i = Object.keys(u);
    else {
        if ("string" != typeof u)
            return new RangeConstraintViolation("Values for " + e + " must be arrays or maps!");
        i = u.split(",").map(function(e) {
            return e.trim()
        })
    }
    switch (eNTITYtYPE.isIntegerType(a) && i.forEach(function(e, u) {
        "string" == typeof e && (i[u] = parseInt(e))
    }),
    a) {
    case "String":
        i.forEach(function(u) {
            "string" != typeof u && (o = new RangeConstraintViolation("Values for " + e + " must be strings!"))
        });
        break;
    case "NonEmptyString":
        i.forEach(function(u) {
            ("string" != typeof u || "" === u.trim()) && (o = new RangeConstraintViolation("Values for " + e + " must be non-empty strings!"))
        });
        break;
    case "Integer":
        i.forEach(function(u) {
            Number.isInteger(u) || (o = new RangeConstraintViolation("The value of " + e + " must be an integer!"))
        });
        break;
    case "NonNegativeInteger":
        i.forEach(function(u) {
            (!Number.isInteger(u) || 0 > u) && (o = new RangeConstraintViolation("The value of " + e + " must be a non-negative integer!"))
        });
        break;
    case "PositiveInteger":
        i.forEach(function(u) {
            (!Number.isInteger(u) || 1 > u) && (o = new RangeConstraintViolation("The value of " + e + " must be a positive integer (and not " + u + ")!"))
        });
        break;
    case "Decimal":
        i.forEach(function(u, t) {
            "string" == typeof u && (i[t] = parseFloat(u)),
            "number" != typeof u && (o = new RangeConstraintViolation("The value of " + e + " must be a (decimal) number!"))
        });
        break;
    case "Boolean":
        i.forEach(function(u) {
            "boolean" != typeof u && (o = new RangeConstraintViolation("The value of " + e + " must be either 'true' or 'false'!"))
        });
        break;
    case "Date":
        i.forEach(function(t) {
            "string" != typeof t || !/\d{4}-(0\d|1[0-2])-([0-2]\d|3[0-1])/.test(t) && isNaN(Date.parse(t)) ? t instanceof Date || (o = new RangeConstraintViolation("The value of " + e + " must be either a Date value or an ISO date string. " + u + " is not admissible!")) : i[r] = new Date(t)
        });
        break;
    default:
        a instanceof eNUMERATION ? i.forEach(function(u) {
            (!Number.isInteger(u) || 1 > u || u > a.MAX) && (o = new RangeConstraintViolation("The value " + u + " is not an admissible enumeration integer for " + e))
        }) : Array.isArray(a) ? i.forEach(function(u) {
            -1 === a.indexOf(u) && (o = new RangeConstraintViolation("The " + e + " value " + u + " is not in value list " + a.toString()))
        }) : a instanceof mODELcLASS ? i.forEach(function(u, t) {
            Number.isInteger(u) && (u = String(u)),
            "string" == typeof u && (void 0 !== a.instances[u] ? (u = a.instances[u],
            i[t] = u) : o = new RangeConstraintViolation(u + " is not a valid ID ref in " + a.name + ".instances, as required for " + e + " !")),
            ("object" != typeof u || void 0 === u.type || u.type !== a) && (o = new RangeConstraintViolation("The value of " + e + " must reference an existing " + a.name + "! " + JSON.stringify(u) + " does not!"))
        }) : "function" == typeof a && i.forEach(function(u) {
            u instanceof a || (o = new RangeConstraintViolation("The value of " + e + " must be a(n) " + a.name + "! " + JSON.stringify(u) + " is not admissible!"))
        })
    }
    if (o)
        return o;
    if (("String" === a || "NonEmptyString" === a) && i.forEach(function(u) {
        void 0 !== b && u.length < b ? o = new StringLengthConstraintViolation("The length of " + e + " must not be smaller than " + b) : void 0 !== t && u.length > t ? o = new StringLengthConstraintViolation("The length of " + e + " must not be greater than " + t) : void 0 === p || p.test(u) || (o = new PatternConstraintViolation(c || u + "does not comply with the pattern defined for " + e))
    }),
    ("Integer" === a || "NonNegativeInteger" === a || "PositiveInteger" === a) && i.forEach(function(u) {
        void 0 !== b && b > u ? o = new IntervalConstraintViolation(e + " must be greater than " + b) : void 0 !== t && u > t && (o = new IntervalConstraintViolation(e + " must be smaller than " + t))
    }),
    o)
        return o;
    if (l > 1) {
        if (d > 0 && i.length < d)
            return new CardinalityConstraintViolation("A collection of at least " + d + " values is required for " + e);
        if (i.length > l)
            return new CardinalityConstraintViolation("A collection value for " + e + " must not have more than " + l + " members!")
    }
    if (n.unique && this.instances)
        for (s = Object.keys(this.instances),
        r = 0; r < s.length; r++)
            if (this.instances[s[r]][e] === u)
                return new UniquenessConstraintViolation("There is already a " + this.name + " with a(n) " + e + " value " + u + "!");
    return n.isStandardId && void 0 === u ? new MandatoryValueConstraintViolation("A value for the standard identifier attribute " + e + " is required!") : (u = 1 === l ? i[0] : i,
    new NoConstraintViolation(u))
}
,
eNTITYtYPE.prototype.checkUniqueness = function(e, u) {
    var t = this.properties[e]
      , a = 0
      , n = [];
    if (t.unique && this.instances)
        for (n = Object.keys(this.instances),
        a = 0; a < n.length; a++)
            if (this.instances[n[a]][e] === u)
                return new UniquenessConstraintViolation("There is already a " + this.name + " with a(n) " + e + " value " + u + "!");
    if (t.isStandardId) {
        if (void 0 === u)
            return new MandatoryValueConstraintViolation("A value for the standard identifier attribute " + label + " is required!");
        if (this.instances && this.instances[u])
            return new UniquenessConstraintViolation("There is already a " + this.name + " with a(n) " + label + " value " + u + "!")
    }
    return new NoConstraintViolation
}
,
eNTITYtYPE.prototype.convertRec2Obj = function(e) {
    var u = {};
    try {
        u = this.create(e)
    } catch (t) {
        console.log(t.constructor.name + " while deserializing a " + this.name + " record: " + t.message),
        u = null
    }
    return u
}
,
mODELcLASS.prototype = Object.create(eNTITYtYPE.prototype),
mODELcLASS.prototype.constructor = mODELcLASS,
mODELcLASS.classes = {},
mODELcLASS.prototype.create = function(e) {
    var u = Object.create(this.methods, this.properties)
      , t = this.properties
      , a = "";
    if (Object.defineProperty(u, "type", {
        value: this,
        writable: !1,
        enumerable: !0
    }),
    Object.keys(e).forEach(function(a) {
        var n = e[a];
        t[a] ? u.set(a, n) : eNTITYtYPE.predefinedAttributes.includes(a) ? u[a] = n : "type" !== a && "continueProcessing" !== a && console.log("Undefined object construction prop: " + a)
    }),
    this.methods.validate && (a = u.validate()))
        throw new ConstraintViolation(this.name + u[this.standardId] + ": " + a);
    return Object.keys(t).forEach(function(e) {
        void 0 === u[e] && void 0 !== t[e].initialValue && (u[e] = t[e].initialValue)
    }),
    u
}
,
sTORAGEmANAGER.prototype.retrieve = function(e, u, t) {
    if ("function" != typeof t)
        throw "sTORAGEmANAGER.prototype.retrieve: the 'continueProcessing' parameter is required!";
    sTORAGEmANAGER.adapters[this.adapter.name].retrieve(e, u, t)
}
,
sTORAGEmANAGER.prototype.retrieveAll = function(e, u) {
    if (e.instances = {},
    "function" != typeof u)
        throw "sTORAGEmANAGER.prototype.retrieveAll: the 'continueProcessing' parameter is required!";
    sTORAGEmANAGER.adapters[this.adapter.name].retrieveAll(e, u)
}
,
sTORAGEmANAGER.prototype.add = function(e, u, t) {
    var a = null ;
    if (t) {
        if ("function" != typeof t)
            throw "sTORAGEmANAGER.prototype.add: 'continueProcessing' must be a function!"
    } else
        t = function(u, t) {
            t ? "string" == typeof t ? console.log(t) : console.log(t.constructor.name + ": " + t.message) : console.log("New " + e.name + u.toString() + " saved.")
        }
        ;
    try {
        a = e.create(u),
        a && sTORAGEmANAGER.adapters[this.adapter.name].add(e, u, a, t)
    } catch (n) {
        n instanceof ConstraintViolation ? console.log(n.constructor.name + ": " + n.message) : console.log(n)
    }
}
,
sTORAGEmANAGER.prototype.update = function(e, u, t, a) {
    function n(n, f) {
        if (f)
            a ? a(null , "There is no " + e.name + " with " + e.standardId + " " + u + " in the database!") : console.log("There is no " + e.name + " with " + e.standardId + " " + u + " in the database!");
        else {
            "object" != typeof n || n.type || (n = e.convertRec2Obj(n)),
            o = util.cloneObject(n);
            try {
                Object.keys(t).forEach(function(e) {
                    var u = n[e]
                      , a = t[e];
                    i[e] && !i[e].isStandardId && (void 0 === i[e].maxCard || 1 === i[e].maxCard ? a !== u && (r.push(e),
                    n.set(e, a)) : (u.length !== a.length || u.some(function(e, u) {
                        return e !== a[u]
                    })) && (n.set(e, a),
                    r.push(e)))
                })
            } catch (d) {
                console.log(d.constructor.name + ": " + d.message),
                s = !1,
                n = o
            }
            s && (r.length > 0 ? (console.log("Properties " + r.toString() + " modified for " + e.name + " " + u),
            sTORAGEmANAGER.adapters[c].update(e, u, t, n, a)) : console.log("No property value changed for " + e.name + " " + u + " !"))
        }
    }
    var o = null
      , i = e.properties
      , r = []
      , s = !0
      , c = this.adapter.name;
    this.retrieve(e, u, n)
}
,
sTORAGEmANAGER.prototype.destroy = function(e, u, t) {
    var a = this.adapter.name;
    this.retrieve(e, u, function(n, o) {
        n ? sTORAGEmANAGER.adapters[a].destroy(e, u, t) : (o = "There is no " + e.name + " with " + e.standardId + " " + u + " in the database!",
        console.log(o))
    })
}
,
sTORAGEmANAGER.prototype.clearData = function(e, u) {
    confirm("Do you really want to delete all data?") && sTORAGEmANAGER.adapters[this.adapter.name].clearData(e, u)
}
,
sTORAGEmANAGER.adapters = {},
sTORAGEmANAGER.adapters.LocalStorage = {
    retrieve: function(e, u, t) {
        t(e.instances[u])
    },
    retrieveAll: function(e, u) {
        function t(e) {
            var u = ""
              , a = []
              , n = 0
              , o = ""
              , i = {}
              , r = util.getTableName(e.name);
            Object.keys(e.properties).forEach(function(u) {
                var a = e.properties[u].range;
                a instanceof mODELcLASS && t(a)
            });
            try {
                localStorage[r] && (o = localStorage[r])
            } catch (s) {
                console.log("Error when reading from Local Storage\n" + s)
            }
            if (o)
                for (i = JSON.parse(o),
                a = Object.keys(i),
                console.log(a.length + " " + e.name + " records loaded."),
                n = 0; n < a.length; n++)
                    u = a[n],
                    e.instances[u] = e.convertRec2Obj(i[u])
        }
        t(e),
        u()
    },
    add: function(e, u, t, a) {
        e.instances[t[e.standardId]] = t,
        this.saveAll(e),
        a(t, null )
    },
    update: function(e, u, t, a, n) {
        this.saveAll(e)
    },
    destroy: function(e, u, t) {
        delete e.instances[u],
        this.saveAll(e)
    },
    clearData: function(e, u) {
        var t = util.getTableName(e.name);
        e.instances = {};
        try {
            localStorage[t] = JSON.stringify({}),
            console.log("Table " + t + " cleared.")
        } catch (a) {
            console.log("Error when writing to Local Storage\n" + a)
        }
    },
    saveAll: function(e) {
        var u = ""
          , t = {}
          , a = null
          , n = 0
          , o = Object.keys(e.instances)
          , i = util.getTableName(e.name);
        for (n = 0; n < o.length; n++)
            u = o[n],
            a = e.instances[u],
            t[u] = a.toRecord();
        try {
            localStorage[i] = JSON.stringify(t),
            console.log(o.length + " " + e.name + " records saved.")
        } catch (r) {
            console.log("Error when writing to Local Storage\n" + r)
        }
    }
};
